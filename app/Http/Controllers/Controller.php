<?php
/**
 * TODO
 */

namespace App\Http\Controllers;

use GitWebhookProxy\ApiClientFactory;
use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Validation;
use App\Jobs\ProjectConfigLoader;


class Controller extends BaseController
{
	/**
	 * Regex for allowed project names.
	 */
	const PROJECTNAME_REGEX = '/[A-Za-z0-9]/';

	/**
	 * The singleton instance of the config loader.
	 *
	 * @var ProjectConfigLoader
	 */
	private $projectConfigLoader;

	/**
	 * The index action sends an overview of the projects and if everything is ready.
	 *
	 * @return Response
	 */
    public function index()
	{
		$content = [];
		try {
			$content = $this->getProjectInformations();
		} catch (\Exception $e) {
			$content = [
				'status' => 'error',
				'message' => $e->getMessage()
			];

			return $this->getResponse($content, 500);
		}

		$globalStatus = ['status' => 'success'];
		$content = array_merge($globalStatus, $content);
		return $this->getResponse($content, 200);
	}


	public function project($name)
	{
		$status = [
			'status'	=> 'failure',
			'message'	=> '',
			'results'	=> []
		];

		if ($this->validateProjectName($name) === false) {
			$status['message'] = 'Project name contains illegal characters';
			return $this->getResponse($status, 500);
		}

		$name = strtolower($name);
		$result = [];
		$exitCode = 0;
		exec($this->getCliCommand($name), $result, $exitCode);
		if ($exitCode === 0) {
			$status['status'] = 'success';
			$status['results'] = $result[0];
			return $this->getResponse($status, 200);
		}

		$status['message'] = implode(PHP_EOL, $result);
		return $this->getResponse($status, 500);


		// TODO: executes the client requests in parallel
//		$thread = new ApiClient();
//		var_dump($thread->wait()->run(ApiClientFactory::API_CLIENT_PACKAGIST, $config[ApiClientFactory::API_CLIENT_PACKAGIST]));

//		$factory = new ApiClientFactory();
//		$apiClient = $factory->getApiClient(ApiClientFactory::API_CLIENT_PACKAGIST, $config[ApiClientFactory::API_CLIENT_PACKAGIST]);
//		$response = $apiClient->doRequest();
//		$status['status'] = 'success';
//		$status['results'][ApiClientFactory::API_CLIENT_PACKAGIST] = json_decode($response->getBody()->getContents());
//		return $this->getResponse($status, $response->getStatusCode());
	}


	/**
	 * Returns the project name extracted by the file informations of a config file.
	 *
	 * @param \SplFileInfo $fileInfo
	 * @return string
	 */
	private function getProjectNameFromConfigFile(\SplFileInfo $fileInfo)
	{
		$search = '.' . $fileInfo->getExtension();
		return str_replace($search, '', $fileInfo->getFilename());
	}

	/**
	 * Collects the general informations of all project configurations.
	 *
	 * @return array
	 */
	private function getProjectInformations()
	{
		$projects = [];
		$configLoader = $this->getProjectConfigLoader();
		$configDir = new \DirectoryIterator($configLoader->getConfigPath());

		/** @var \DirectoryIterator $configFile */
		foreach ($configDir as $configFile) {
			$fileInfo = $configFile->getFileInfo();
			if ($fileInfo->getExtension() !== ProjectConfigLoader::CONFIG_FILE_EXTENSION || $fileInfo->isFile() === false) {
				continue;
			}

			$projectName = $this->getProjectNameFromConfigFile($fileInfo);
			$config = $configLoader->loadConfig($fileInfo->getPathname());
			$projects[$projectName] = array_keys($config);
		}
		return $projects;
	}

	/**
	 * Returns the response with content and status code. It's fixed that the response is always a json response, so
	 * content is converted to json automatically.
	 *
	 * @param mixed $content The content to return.
	 * @param int $code The status code to return.
	 * @return Response
	 */
	private function getResponse($content, $code)
	{
		return new Response(json_encode($content), $code, [
			'content-type' => 'application/json'
		]);
	}

	/**
	 * Validate the projects name. Returns true if it's valid.
	 *
	 * @param string $name The name of the project.
	 * @return bool
	 */
	private function validateProjectName($name)
	{
		$validator = Validation::createValidator();

		$constraints = new Constraints\Collection([
			'name' => [
				new Constraints\Regex(self::PROJECTNAME_REGEX),
				new Constraints\Length([
					'min' => 3,
					'max' => 50
				])
			]
		]);

		$violations = $validator->validate(['name' => $name], $constraints);
		if ($violations->count() === 0) {
			return true;
		}

		return false;
	}

	/**
	 * Returns the cli command by a given project name.
	 *
	 * @param string $name The name of the project.
	 * @return string
	 */
	private function getCliCommand($name)
	{
		$command = [
			'php',
			realpath(dirname(__FILE__) . '/../../../') . '/artisan',
			'project:runner',
			'--name=' . $name
		];
		return implode(' ', $command);
	}

	/**
	 * Returns the config loader.
	 *
	 * @return ProjectConfigLoader
	 */
	private function getProjectConfigLoader()
	{
		if ($this->projectConfigLoader === null) {
			$this->projectConfigLoader = new ProjectConfigLoader();
		}
		return $this->projectConfigLoader;
	}
}
