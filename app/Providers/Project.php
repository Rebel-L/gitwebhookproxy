<?php
/**
 * The provider for the project commands.
 *
 * @category App
 * @package Providers
 *
 * @author Rebel-L <dj@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version 1.0.0
 * @version GIT: $Id$ Stable.
 * 
 * Date: 13.09.2015
 * Time: 16:34
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Console\Commands\ProjectRunner;

class Project extends ServiceProvider
{
	/**
	 * The provider name.
	 */
	const COMMAND_PROVIDER_NAME = 'command.project.runner';

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton(self::COMMAND_PROVIDER_NAME, function()
		{
			return new ProjectRunner();
		});

		$this->commands(self::COMMAND_PROVIDER_NAME);
	}
}