<?php
/**
 * Class is responsible to load the project config files.
 *
 * @category App
 * @package Jobs
 *
 * @author Rebel-L <dj@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version 1.0.0
 * @version GIT: $Id$ Stable.
 * 
 * Date: 13.09.2015
 * Time: 18:04
 */

namespace App\Jobs;


class ProjectConfigLoader
{
	/**
	 * The relative path to the configs.
	 */
	const CONFIG_PATH = '/../../etc';

	/**
	 * The extension of the config files.
	 */
	const CONFIG_FILE_EXTENSION = 'php';

	/**
	 * Returns the path to the configs.
	 *
	 * @return string
	 */
	public function getConfigPath()
	{
		return realpath(dirname(__FILE__) . self::CONFIG_PATH) . DIRECTORY_SEPARATOR;
	}

	/**
	 * Loads the config and returns the array included.
	 *
	 * @param string $fileName The filename of the config file to load.
	 * @return array
	 */
	public function loadConfig($fileName)
	{
		return include $fileName;
	}

	/**
	 * Loads the config for a specific project.
	 * <b style="color: #F00;">Throws exception if configuration file is not found.</b>
	 *
	 * @param string $name The name of the project.
	 * @throws RuntimeException
	 * @return array
	 */
	public function loadConfigByProjectName($name)
	{
		$fileName = $this->getConfigPath() . $name . '.' . self::CONFIG_FILE_EXTENSION;
		if (file_exists($fileName) === false) {
			throw new \RuntimeException('No configuration found for project: ' . $name);
		}

		return $this->loadConfig($fileName);
	}
}