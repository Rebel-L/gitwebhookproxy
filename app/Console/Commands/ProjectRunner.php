<?php
/**
 * TODO: description
 *
 * @category TODO
 * @package TODO
 *
 * @author Rebel-L <dj@rebel@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version TODO
 * @version GIT: $Id$ In development. Very unstable.
 * 
 * Date: 13.09.2015
 * Time: 16:30
 */

namespace App\Console\Commands;

use App\Jobs\ProjectConfigLoader;
use \Aza\Components\Thread\Thread;
use Aza\Components\Thread\ThreadPool;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class ProjectRunner extends Command
{
	/**
	 * The default value for maximum threads to be used.
	 */
	const MAX_THREADS_DEFAULT = 8;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'project:runner';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This executes all projects api calls in parallel.';

	/**
	 * The singleton instance of the config loader.
	 *
	 * @var ProjectConfigLoader
	 */
	private $projectConfigLoader;

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		/**
		 * Initialize parameters
		 */
		Thread::$useForks = true;
		$name = $this->option('name');

		$maxThreads = $this->option('maxThreads');
		if ($maxThreads === null) {
			$maxThreads = self::MAX_THREADS_DEFAULT;
		}

		$status = [
			'status'	=> 'failure',
			'code'		=> 500,
			'message'	=> '',
			'results'	=> []
		];

		/**
		 * Load project config
		 */
		$projectConfig = [];
		try {
			$projectConfig = $this->getProjectConfigLoader()->loadConfigByProjectName($name);
		} catch (\RuntimeException $e) {
			$status['message'] = $e->getMessage();
			$status['code'] = 404;
			$this->error(json_encode($status));
			return;
		}

		/**
		 * Do the work
		 */
		$jobsTotal = count($projectConfig);
		$jobsLeft = $jobsTotal;
		$threadPool = new ThreadPool('\App\Jobs\ApiClient', $maxThreads);
		do {
			while ($jobsLeft > 0 && $threadPool->hasWaiting() === true) {
				$jobsLeft--;
				$threadPool->run($projectConfig[$jobsLeft]);
			}

			$failed = [];
			$results = $threadPool->wait($failed);
			foreach ($results as $threadId => $result) {
				$status['result'][] = [
					'ThreadId'	=> $threadId,
					'Result'	=> $result
				];
				$jobsTotal--;
			}

			// TODO: what do we do with failed jobs?
		} while ($jobsTotal > 0);
		$threadPool->cleanup();

		$status['status'] = 'success';
		$status['code']	  = 200;
		$this->info(json_encode($status));
	}

	/**
	 * @see \Illuminate\Console\Command::getOptions
	 * @inheritdoc
	 */
	protected function getOptions()
	{
		/**
		 * project name
		 */
		$projectName = [
			'name',
			null,
			InputOption::VALUE_REQUIRED,
			'The name of the project to execute the API calls.'
		];

		/**
		 * maximum threads
		 */
		$maxThreads = [
			'maxThreads',
			null,
			InputOption::VALUE_OPTIONAL,
			'The maximum number of threads to be used'
		];

		return [
			$projectName,
			$maxThreads
		];
	}

	/**
	 * Returns the config loader.
	 *
	 * @return ProjectConfigLoader
	 */
	private function getProjectConfigLoader()
	{
		if ($this->projectConfigLoader === null) {
			$this->projectConfigLoader = new ProjectConfigLoader();
		}
		return $this->projectConfigLoader;
	}
}